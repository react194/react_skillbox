import React from 'react';
// @ts-ignore
import styles from './style.ts';
import { EIcons } from '../../assets/icons';

interface IIconProps {
  name: string;
  width: number;
  height: number;
  viewBox?: string;
}

export function Icon({name, width, height, viewBox}: IIconProps) {
  const svgRef = React.useRef<SVGSVGElement | null>(null);

  React.useEffect(() => {
    const svgElement = svgRef.current as SVGElement;

    svgElement.innerHTML = EIcons[name];
  }, [name])

  return (
    <svg ref={svgRef} xmlns="http://www.w3.org/2000/svg" fill="none" width={width} height={height} viewBox={viewBox}></svg>
  );
}
