import React from 'react';
// @ts-ignore
import CardUserHeader from './style.ts';

export function UserHeader() {
  return (
    <CardUserHeader>
      <img src="https://img.freepik.com/free-vector/hand-drawn-wolf-outline-illustration_23-2149256565.jpg?t=st=1679585157~exp=1679585757~hmac=2a1b53821e8b68df7f400809566008deab2ac5025b4fe8d76d69ef56d2f6584a" alt="" />
      <p>Константин Кодов</p>
      <p><span>опубликовано</span> 4 часа назад</p>
    </CardUserHeader>
  );
}
