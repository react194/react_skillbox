import React from 'react';
// @ts-ignore
import NewsTitleWrapper from './style.ts';

export function NewsTitle() {
  return (
    <NewsTitleWrapper>
      Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi recusandae nobis, consectetur iure ipsam dolores, magni sed quos, provident perspiciatis dolor possimus. Quidem officiis voluptate a at eaque esse fugit.
    </NewsTitleWrapper>
  );
}
