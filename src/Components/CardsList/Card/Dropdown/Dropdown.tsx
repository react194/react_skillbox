import React from 'react';
// @ts-ignore
import DropdownWrapper from './style.ts';

type TTarget = {
  props: {
    style?: string
  }
}

interface IDropdownProps {
  target: TTarget | React.ReactNode;
  children: React.ReactNode;
  isOpen?: Boolean;
  onOpen?: () => void;
  onClose?: () => void;
}

const NOOP = () => {};

export function Dropdown({target, children, isOpen, onOpen = NOOP, onClose = NOOP}: IDropdownProps) {
  const [isDropdownOpen, setIsDropdownOpen] = React.useState(isOpen);

  React.useEffect(() => setIsDropdownOpen(isOpen), [isOpen]);
  React.useEffect(() => isDropdownOpen ? onOpen() : onClose() , [isDropdownOpen])

  const handlerOpen = () => {
    if (isOpen === undefined) {
      setIsDropdownOpen(!isDropdownOpen)
    }
  }

  return (
    <DropdownWrapper>
      <div onClick={handlerOpen}>
        {target}
      </div>
      {isDropdownOpen && (
        <div>
          <div onClick={() => setIsDropdownOpen(false)}>
            {children}
          </div>
          <button onClick={() => setIsDropdownOpen(false)}>Закрыть</button>
        </div>
      )}
    </DropdownWrapper>
  );
}
