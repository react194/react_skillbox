import React from 'react';
// @ts-ignore
import NavWrapper from './style.ts';
import {Raiting} from './Raiting';
import {Comments} from './Comments';
import {Buttons} from './Buttons';

export function Nav() {
  return (
    <NavWrapper>
      <Raiting />
      <Comments />
      <Buttons />
    </NavWrapper>
  );
}
