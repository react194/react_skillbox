import React from 'react';
// @ts-ignore
import RatingWrapper from './style.ts';
import { Icon } from '../../../../Icon';

export function Raiting() {
  return (
    <RatingWrapper>
      <div data-rotate="up">
        <Icon name={'ArrowIcon'} width={19} height={10} />
      </div>
      <p>101</p>
      <div data-rotate="down">
        <Icon name={'ArrowIcon'} width={19} height={10} />
      </div>
    </RatingWrapper>
  );
}
