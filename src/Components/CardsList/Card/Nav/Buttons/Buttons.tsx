import React from 'react';
// @ts-ignore
import ButtonsWrapper from './style.ts';
import { Icon } from '../../../../Icon';

export function Buttons() {
  return (
    <ButtonsWrapper>
      <Icon name={'RaitingIcon'} width={20} height={20} />
      <Icon name={'ForwardIcon'} width={20} height={20} />
      <Icon name={'RemoveIcon'} width={20} height={20} />
      <Icon name={'AddIcon'} width={20} height={20} />
    </ButtonsWrapper>
  );
}
