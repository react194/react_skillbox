import React from 'react';
// @ts-ignore
import CommentsWrapper from './style.ts';
import { Icon } from '../../../../Icon';

export function Comments() {
  return (
    <CommentsWrapper>
      <Icon name={'CommentsIcon'} width={15} height={15} />
      14
    </CommentsWrapper>
  );
}