import React from 'react';
// @ts-ignore
import PictureWrapper from './style.ts';

export function Picture() {
  return (
    <PictureWrapper>
      <img src="https://img.freepik.com/free-photo/beautiful-view-of-tourist-boat-sailing-through-icebergs-in-disko-bay-greenland_181624-49963.jpg?t=st=1679583366~exp=1679583966~hmac=c37ede4f633d4697fd2b0af735b0eaf59abdd5babc51caf5531cc88f92815088" alt="" />
    </PictureWrapper>
  );
}
