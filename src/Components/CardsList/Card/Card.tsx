import React from 'react';
// @ts-ignore
import CardWrapper from './style.ts';
import {Tooltip} from './Tooltip'
import {UserHeader} from './UserHeader'
import {NewsTitle} from './NewsTitle';
import {Picture} from './Ricture';
import {Nav} from './Nav';
import { Dropdown } from './Dropdown';
import { Button } from './Tooltip/Button';
import { nanoid } from 'nanoid';
import {isMobile} from 'react-device-detect';
import { Icon } from '../../Icon';

const ButtonTooltipList = [
  {text: 'Комментарии', onClick: () =>  console.log('Комментарии'), image: <Icon name={'CommentsIcon'} width={15} height={15} />, isMobile: !isMobile},
  {text: 'Поделиться', onClick: () =>  console.log('Поделиться'), image: <Icon name={'ForwardDvIcon'} width={12} height={14} />, isMobile: !isMobile},
  {text: 'Скрыть', onClick: () =>  console.log('Скрыть'), image: <Icon name={'HideDvIcon'} width={14} height={14} />, isMobile: true},
  {text: 'Сохранить', onClick: () =>  console.log('Сохранить'), image: <Icon name={'SaveDvIcon'} width={14} height={14} />, isMobile: !isMobile},
  {text: 'Пожаловаться', onClick: () =>  console.log('Пожаловаться'), image: <Icon name={'ClaimDvIcon'} width={16} height={14} />, isMobile: true},
];

export function Card() {
  return (
    <CardWrapper>
      <UserHeader />
      <NewsTitle />
      <Picture />
      <Dropdown target={<Tooltip />}>
        {ButtonTooltipList.map(({text, onClick, image, isMobile}) => (
          <Button image={image} isMobile={isMobile} text={text} onClick={onClick} key={nanoid()} />
        ))}
      </Dropdown>
      <Nav />
    </CardWrapper>
  );
}
