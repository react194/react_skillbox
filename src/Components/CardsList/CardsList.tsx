import React from 'react';
// @ts-ignore
import CardsListWrapper from './style.ts';
import { Card } from './Card';

export function CardsList() {
  return (
    <CardsListWrapper>
      <Card />
      <Card />
    </CardsListWrapper>
  );
}
