import { hot } from "react-hot-loader/root";
import * as React from "react";
import GlobalStyle from './globalStyles';
import { Layout } from './Components/Layout';
import { Header } from './Components/Header';
import { Content } from './Components/Content';
import { CardsList } from './Components/CardsList';

function AppComponent() {
  return (
    <>
      <GlobalStyle />
      <Layout>
        <Header />
        <Content>
          <CardsList />
        </Content>
      </Layout>
    </>
  );
}

export const App = hot(AppComponent);
